<?php
require_once 'core/PasswordPhraseGenerator.php';

$sentence = new PasswordPhraseGenerator;

$result = $sentence->generate();

?>

<!DOCTYPE html>
<html lang="da">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sætningsgenerator</title>
	<style>
		* {
			margin: 0;
			padding: 0;
		}

		html {
			height: 100%;
		}

		h1 {
			text-align: center;
			margin-top: 10%;
		}

		.the-result {
			background: #333;
			color: #FFF;
			padding: 0.3em;
		}
	</style>
</head>

<body>

	<h1>
		<span class="the-result"><?=$result?></span>
	</h1>
</body>

</html>