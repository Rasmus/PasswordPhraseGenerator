<?php

/**
 * PasswordPhraseGenerator
 */
class PasswordPhraseGenerator
{
    /**
     * Initializing object
     */
    function __construct()
    {
        $this->sub = json_decode(file_get_contents('data/navneord.json'));
        $this->ver = json_decode(file_get_contents('data/verber.json'));
        $this->adj = json_decode(file_get_contents('data/adj.json'));
        $this->ord = [
            "sub" => json_decode(file_get_contents('data/navneord.json')),
            "ver" => json_decode(file_get_contents('data/verber.json')),
            "adj" => json_decode(file_get_contents('data/adj.json')),
            "pro" => ["jeg", "han", "hun", "den", "det", "de", "I", "vi", "du", "De"],
            "tid" => ["før", "efter", "imens", "inden"],
            "for" => ["over", "under", "ved siden af", "bag", "foran", "overfor"],
        ];
        $this->tal = rand(2, 79);
        $this->model = rand(0, 1);
        $this->end = array(".", "!", "?", "!?", "...", " :-)", " ;-)");
        $this->tegn = $this->end[array_rand($this->end)];
    }


    /**
     * Random sub element
     *
     * @param array $arr
     * @return array
     */
    private function _rando($arr)
    {
        return $arr[array_rand($arr)];
    }

    /**
     * Select random word from class
     *
     * @param string $cla
     * @param int $conj
     * @return string $word
     */
    private function _word($cla, $conj = null)
    {
        $word = $this->ord[$cla][array_rand($this->ord[$cla])]; //rand(0,count($ord[$cla]));
        if ($conj == null) {
            return $word;
        } else {
            return $word[$conj];
        }
    }

    /**
     * Main model generator
     *
     * @param integer $conj
     * @return string $phrase
     */
    function lego($conj = 0)
    {
        // 0 = ubestemt ental, eks. "en grim tyr" el. "et dumt æg".
        // 1 = bestemt ental, eks. "den grimme tyr" el. "det dumme æg"
        // 2 = ubestemt flertal, eks. "ægte tedåser"
        // 3 = bestemt flertal, eks. "de rådne æbler"
        $main = $this->_word("sub");
        $desc = $this->_word("adj");
        if ($main[4] == "t") {
            $neutrum = true;
        } else {
            $neutrum = false;
        }

        switch ($conj) {
            case 0:
                if ($neutrum) {
                    $phrase = "et " . $this->_word("adj")[1] . " " . $main[0];
                } else {
                    $phrase = "en " . $this->_word("adj")[0] . " " . $main[0];
                }
                break;
            case 1:
                if ($neutrum) {
                    $phrase = "det " . $this->_word("adj")[2] . " " . $main[0];
                } else {
                    $phrase = "den " . $this->_word("adj")[2] . " " . $main[0];
                }
                break;
            case 2:
                $phrase = $this->_word("adj")[2] . " " . $main[2];
                break;
            case 3:
                $phrase = "de " . $this->_word("adj")[2] . " " . $main[2];
                break;
        }
        return $phrase;
    }

    /**
     * Generate phrase
     *
     * @return string $phrase
     */
    public function generate()
    {
        switch ($this->model) {
            case 0:
                if ($this->tal > 1) {
                    $b = $this->lego(2);
                } else {
                    $b = $this->lego(rand(0, 1));
                }
                $c = $this->_word("ver", rand(1, 4));
                $d = $this->_word("sub", $this->_rando([1, 3]));
                if (rand(0, 1) > 0) {
                    $which = rand(1, 3);
                    switch ($which) {
                        case 1:
                            $b = mb_strtoupper($b);
                            break;
                        case 2:
                            $c = mb_strtoupper($c);
                            break;
                        case 1:
                            $d = mb_strtoupper($d);
                            break;
                    }
                }
                $result = "{$this->tal} $b $c $d{$this->tegn}";
                break;
            case 1:
                $a = $this->lego($this->_rando([0, 1, 3]));
                $b = $this->_word("ver", rand(1, 4));
                if ($this->tal > 1) {
                    $c = $this->_word("sub", 2);
                } else {
                    $c = $this->_word("sub", $this->_rando([1, 3]));
                }
                $result = "$a $b {$this->tal} $c{$this->tegn}";
                break;
        }

        return ucwords($result);
    }
}
